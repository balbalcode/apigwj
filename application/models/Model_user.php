<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user extends CI_Model {
	private $table_1 = 'user';

	public function get_user($userId='')
	{
		if ($userId) {
			$this->db->where('userId', $userId);
		}

		$this->db->select('*');
		$this->db->from($this->table_1);

		$data = $this->db->get();
		return $data->row();
	}

	public function insert_user($data='')
	{
		$query = $this->db->insert($this->table_1, $data);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}

	public function update_user($userId='', $data)
	{
		$this->db->where('userId', $userId);
		$query = $this->db->update($this->table_1,$data);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}

	public function delete_user($userId='')
	{
		$this->db->where('userId', $userId);
		$query = $this->db->delete($this->table_1);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}

	

}

/* End of file model_user.php */
/* Location: ./application/models/model_user.php */