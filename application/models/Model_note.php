<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_note extends CI_Model {
	private $table_1 = 'note';

	public function get_note_user($userId='')
	{
		if ($userId) {
			$this->db->where('userId', $userId);
		}

		$this->db->select('*');
		$this->db->from($this->table_1);

		$data = $this->db->get();
		return $data->result();
	}

	public function get_detail_note($noteId='')
	{
		if ($noteId) {
			$this->db->where('noteId', $noteId);
		}

		$this->db->select('*');
		$this->db->from($this->table_1);

		$data = $this->db->get();
		return $data->row();
	}

	public function insert_note($data='')
	{
		$query = $this->db->insert($this->table_1, $data);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}

	public function update_note($noteId='', $data)
	{
		$this->db->where('noteId', $noteId);
		$query = $this->db->update($this->table_1,$data);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}


	public function delete_note($noteId='')
	{
		$this->db->where('noteId', $noteId);
		$query = $this->db->delete($this->table_1);

		if ($this->db->affected_rows() == 1) {
			return true;
		}else{
			return false;
		}
	}

	

}

/* End of file model_note.php */
/* Location: ./application/models/model_note.php */