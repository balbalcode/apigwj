<?php

defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class ReminderService extends REST_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_reminder', 'mr');
	}


	public function index_get()
	{
		$responseCode = "X5";
		$responseDesc = "Failed to load data";
		$responseData = null;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function getReminder_get($userId)
	{
		$data = $this->mr->get_reminder_user($userId);

		$responseCode = "200";
		$responseDesc = "Success get reminder";
		$responseData = $data;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}


	public function getReminderById_get($reminderId)
	{
		$data = $this->mr->get_detail_reminder($reminderId);

		$responseCode = "200";
		$responseDesc = "Success get reminder";
		$responseData = $data;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function getReminderBySchedule_get($scheduleId)
	{
		$data = $this->mr->get_schedule_reminder($scheduleId);

		$responseCode = "200";
		$responseDesc = "Success get reminder";
		$responseData = $data;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function createReminder_post()
	{

		$post = $this->post();

		$this->form_validation->set_data($post);
		$this->form_validation->set_rules('reminderDate','Date','required');
		$this->form_validation->set_rules('reminderContent','Content','required');
		$this->form_validation->set_rules('userId','User','required');
		$responseData = null;
		if ($this->form_validation->run() == TRUE) {

			$insertData = array(
				'reminderDate' => $this->post('reminderDate'),
				'reminderContent' => $this->post('reminderContent'),
				'userId' => $this->post('userId')
			);

			$query = $this->mr->insert_reminder($insertData);

			if ($query) {
				$responseCode = "00";
				$responseDesc = "Success to create reminder";
			}
			else{
				$responseCode = "01";
				$responseDesc = "Failed to create reminder";
			}

		}
		else{
			$responseCode = "401";
			$responseDesc = $this->form_validation->error_array();
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}


	public function updateReminder_put($reminderId)
	{

		$post = $this->put();

		$this->form_validation->set_data($post);
		$this->form_validation->set_rules('reminderDate','Date','required');
		$this->form_validation->set_rules('reminderContent','Content','required');
		$this->form_validation->set_rules('userId','User','required');

		$responseData = null;
		if ($this->form_validation->run() == TRUE) {

			$updateData = array(
				'reminderDate' => $this->put('reminderDate'),
				'reminderContent' => $this->put('reminderContent'),
				'userId' => $this->put('userId')
			);

			$query = $this->mr->update_reminder($reminderId, $updateData);

			if ($query) {
				$responseCode = "00";
				$responseDesc = "Success to update reminder";
			}
			else{
				$responseCode = "01";
				$responseDesc = "Failed to update reminder";
			}

		}
		else{
			$responseCode = "401";
			$responseDesc = $this->form_validation->error_array();
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function deleteReminder_delete($reminderId)
	{

		$query = $this->mr->delete_reminder($reminderId);

		if ($query) {
			$responseCode = "00";
			$responseDesc = "Success to delete reminder";
			$responseData = null;
		}

		else{
			$responseCode = "01";
			$responseDesc = "Failed to delete reminder";
			$responseData = null;
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);

	}




}
