<?php

defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class NoteService extends REST_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_note', 'mn');
	}


	public function index_get()
	{
		$responseCode = "X5";
		$responseDesc = "Failed to load data";
		$responseData = null;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function getNote_get($userId)
	{
		$data = $this->mn->get_note_user($userId);

		$responseCode = "200";
		$responseDesc = "Success get note";
		$responseData = $data;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}


	public function getNoteById_get($noteId)
	{
		$data = $this->mn->get_detail_note($noteId);

		$responseCode = "200";
		$responseDesc = "Success get note";
		$responseData = $data;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function createNote_post()
	{

		$post = $this->post();

		$this->form_validation->set_data($post);
		$this->form_validation->set_rules('noteDate','Date','required');
		$this->form_validation->set_rules('noteTitle','Title','required');
		$this->form_validation->set_rules('noteContent','Content','required');
		$this->form_validation->set_rules('userId','User','required');
		$responseData = null;
		if ($this->form_validation->run() == TRUE) {

			$insertData = array(
				'noteDate' => $this->post('noteDate'),
				'noteTitle' => $this->post('noteTitle'),
				'noteContent' => $this->post('noteContent'),
				'userId' => $this->post('userId')
			);

			$query = $this->mn->insert_note($insertData);

			if ($query) {
				$responseCode = "00";
				$responseDesc = "Success to create note";
			}
			else{
				$responseCode = "01";
				$responseDesc = "Failed to create note";
			}

		}
		else{
			$responseCode = "401";
			$responseDesc = $this->form_validation->error_array();
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}


	public function updateNote_put($noteId)
	{

		$post = $this->put();

		$this->form_validation->set_data($post);
		$this->form_validation->set_rules('noteDate','Date','required');
		$this->form_validation->set_rules('noteTitle','Title','required');
		$this->form_validation->set_rules('noteContent','Content','required');
		$this->form_validation->set_rules('userId','User','required');
		

		$responseData = null;
		if ($this->form_validation->run() == TRUE) {

			$updateData = array(
				'noteDate' => $this->put('noteDate'),
				'noteTitle' => $this->put('noteTitle'),
				'noteContent' => $this->put('noteContent'),
				'userId' => $this->put('userId')
			);

			$query = $this->mn->update_note($noteId, $updateData);

			if ($query) {
				$responseCode = "00";
				$responseDesc = "Success to update note";
			}
			else{
				$responseCode = "01";
				$responseDesc = "Failed to update note";
			}

		}
		else{
			$responseCode = "401";
			$responseDesc = $this->form_validation->error_array();
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function deleteNote_delete($noteId)
	{

		$query = $this->mn->delete_note($noteId);

		if ($query) {
			$responseCode = "00";
			$responseDesc = "Success to delete note";
			$responseData = null;
		}

		else{
			$responseCode = "01";
			$responseDesc = "Failed to delete note";
			$responseData = null;
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);

	}




}
