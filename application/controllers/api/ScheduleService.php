<?php

defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class scheduleService extends REST_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_schedule', 'ms');
	}


	public function index_get()
	{
		$responseCode = "X5";
		$responseDesc = "Failed to load data";
		$responseData = null;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function getSchedule_get($userId)
	{
		$data = $this->ms->get_schedule_user($userId);

		$responseCode = "200";
		$responseDesc = "Success get schedule";
		$responseData = $data;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}


	public function getScheduleById_get($scheduleId)
	{
		$data = $this->ms->get_detail_schedule($scheduleId);

		$responseCode = "200";
		$responseDesc = "Success get schedule";
		$responseData = $data;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function createSchedule_post()
	{

		$post = $this->post();

		$this->form_validation->set_data($post);
		$this->form_validation->set_rules('scheduleDate','Date','required');
		$this->form_validation->set_rules('scheduleContent','Content','required');
		$this->form_validation->set_rules('userId','User','required');
		$responseData = null;
		if ($this->form_validation->run() == TRUE) {

			$insertData = array(
				'scheduleDate' => $this->post('scheduleDate'),
				'scheduleContent' => $this->post('scheduleContent'),
				'userId' => $this->post('userId')
			);
			$scheduleQuery = $this->ms->insert_schedule($insertData);

			$autoRemind = array(
				'reminderDate' => date('Y-m-d'),
				'reminderContent' => 'Auto remind by schehule create',
				'scheduleId' => $scheduleQuery['scheduleId'],
				'userId' => $this->post('userId')
			);

			$query = $this->ms->create_reminder($autoRemind);

			if ($query) {
				$responseCode = "00";
				$responseDesc = "Success to create schedule";
			}
			else{
				$responseCode = "01";
				$responseDesc = "Failed to create schedule";
			}

		}
		else{
			$responseCode = "401";
			$responseDesc = $this->form_validation->error_array();
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}


	public function updateSchedule_put($scheduleId)
	{

		$post = $this->put();

		$this->form_validation->set_data($post);
		$this->form_validation->set_rules('scheduleDate','Date','required');
		$this->form_validation->set_rules('scheduleContent','Content','required');
		$this->form_validation->set_rules('userId','User','required');

		$responseData = null;
		if ($this->form_validation->run() == TRUE) {

			$updateData = array(
				'scheduleDate' => $this->put('scheduleDate'),
				'scheduleContent' => $this->put('scheduleContent'),
				'userId' => $this->put('userId')
			);

			$query = $this->ms->update_schedule($scheduleId, $updateData);

			if ($query) {
				$responseCode = "00";
				$responseDesc = "Success to update schedule";
			}
			else{
				$responseCode = "01";
				$responseDesc = "Failed to update schedule";
			}

		}
		else{
			$responseCode = "401";
			$responseDesc = $this->form_validation->error_array();
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function deleteSchedule_delete($scheduleId)
	{

		$query = $this->ms->delete_schedule($scheduleId);

		if ($query) {
			$responseCode = "00";
			$responseDesc = "Success to delete schedule";
			$responseData = null;
		}

		else{
			$responseCode = "01";
			$responseDesc = "Failed to delete schedule";
			$responseData = null;
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);

	}




}
