<?php

defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class ContactService extends REST_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_contact', 'mc');
	}


	public function index_get()
	{
		$responseCode = "X5";
		$responseDesc = "Failed to load data";
		$responseData = null;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function getContact_get($userId)
	{
		$data = $this->mc->get_contact_user($userId);

		$responseCode = "200";
		$responseDesc = "Success get contact";
		$responseData = $data;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}


	public function getContactById_get($contactId)
	{
		$data = $this->mc->get_detail_contact($contactId);

		$responseCode = "200";
		$responseDesc = "Success get contact";
		$responseData = $data;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function createContact_post()
	{

		$post = $this->post();

		$this->form_validation->set_data($post);
		$this->form_validation->set_rules('contactAddress','Address','required');
		$this->form_validation->set_rules('contactDivision','Division','required');
		$this->form_validation->set_rules('contactName','Name','required');
		$this->form_validation->set_rules('contactNumber','Number','required');
		$this->form_validation->set_rules('contactEmail','Email','required');
		$this->form_validation->set_rules('contactCompany','Company Name','required');
		$this->form_validation->set_rules('userId','User','required');
		$responseData = null;
		if ($this->form_validation->run() == TRUE) {

			$insertData = array(
				'contactAddress' => $this->post('contactAddress'),
				'contactDivision' => $this->post('contactDivision'),
				'contactName' => $this->post('contactName'),
				'contactNumber' => $this->post('contactNumber'),
				'contactEmail' => $this->post('contactEmail'),
				'contactCompany' => $this->post('contactCompany'),
				'userId' => $this->post('userId')
			);

			$query = $this->mc->insert_contact($insertData);

			if ($query) {
				$responseCode = "00";
				$responseDesc = "Success to create contact";
			}
			else{
				$responseCode = "01";
				$responseDesc = "Failed to create contact";
			}

		}
		else{
			$responseCode = "401";
			$responseDesc = $this->form_validation->error_array();
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}


	public function updateContact_put($contactId)
	{

		$post = $this->put();

		$this->form_validation->set_data($post);
		$this->form_validation->set_rules('contactAddress','Address','required');
		$this->form_validation->set_rules('contactDivision','Division','required');
		$this->form_validation->set_rules('contactName','Date','required');
		$this->form_validation->set_rules('contactNumber','Number','required');
		$this->form_validation->set_rules('contactEmail','Email','required');
		$this->form_validation->set_rules('contactCompany','Company Name','required');
		$this->form_validation->set_rules('userId','User','required');
		

		$responseData = null;
		if ($this->form_validation->run() == TRUE) {

			$updateData = array(
				'contactAddress' => $this->put('contactAddress'),
				'contactDivision' => $this->put('contactDivision'),
				'contactName' => $this->put('contactName'),
				'contactNumber' => $this->put('contactNumber'),
				'contactEmail' => $this->put('contactEmail'),
				'contactCompany' => $this->put('contactCompany'),
				'userId' => $this->put('userId')
			);

			$query = $this->mc->update_contact($contactId, $updateData);

			if ($query) {
				$responseCode = "00";
				$responseDesc = "Success to update contact";
			}
			else{
				$responseCode = "01";
				$responseDesc = "Failed to update contact";
			}

		}
		else{
			$responseCode = "401";
			$responseDesc = $this->form_validation->error_array();
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function deleteContact_delete($contactId)
	{

		$query = $this->mc->delete_contact($contactId);

		if ($query) {
			$responseCode = "00";
			$responseDesc = "Success to delete contact";
			$responseData = null;
		}

		else{
			$responseCode = "01";
			$responseDesc = "Failed to delete contact";
			$responseData = null;
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);

	}




}
