<?php

defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class NewsService extends REST_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_news', 'mn');
	}


	public function index_get()
	{
		$responseCode = "X5";
		$responseDesc = "Failed to load data";
		$responseData = null;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function getNews_get($userId)
	{
		$data = $this->mn->get_news_user($userId);

		$responseCode = "200";
		$responseDesc = "Success get news";
		$responseData = $data;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}


	public function getNewsById_get($newsId)
	{
		$data = $this->mn->get_detail_news($newsId);

		$responseCode = "200";
		$responseDesc = "Success get news";
		$responseData = $data;

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function createNews_post()
	{

		$post = $this->post();

		$this->form_validation->set_data($post);
		$this->form_validation->set_rules('newsTitle','Title','required');
		$this->form_validation->set_rules('newsText','Content','required');
		$this->form_validation->set_rules('newsDate','Date','required');
		$this->form_validation->set_rules('newsStatus','Status','required');
		$this->form_validation->set_rules('userId','User','required');

		$responseData = null;
		if ($this->form_validation->run() == TRUE) {

			$insertData = array(
				'newsTitle' => $this->post('newsTitle'),
				'newsText' => $this->post('newsText'),
				'newsDate' => $this->post('newsDate'),
				'newsStatus' => $this->post('newsStatus'),
				'userId' => $this->post('userId')
			);

			$query = $this->mn->insert_news($insertData);

			if ($query) {
				$responseCode = "00";
				$responseDesc = "Success to create news";
			}
			else{
				$responseCode = "01";
				$responseDesc = "Failed to create news";
			}

		}
		else{
			$responseCode = "401";
			$responseDesc = $this->form_validation->error_array();
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}


	public function updateNews_put($newsId)
	{

		$post = $this->put();

		$this->form_validation->set_data($post);
		$this->form_validation->set_rules('newsTitle','Title','required');
		$this->form_validation->set_rules('newsText','Content','required');
		$this->form_validation->set_rules('newsDate','Date','required');
		$this->form_validation->set_rules('newsStatus','Status','required');
		$this->form_validation->set_rules('userId','User','required');

		$responseData = null;
		if ($this->form_validation->run() == TRUE) {

			$updateData = array(
				'newsTitle' => $this->put('newsTitle'),
				'newsText' => $this->put('newsText'),
				'newsDate' => $this->put('newsDate'),
				'newsStatus' => $this->put('newsStatus'),
				'userId' => $this->put('userId')
			);

			$query = $this->mn->update_news($newsId, $updateData);

			if ($query) {
				$responseCode = "00";
				$responseDesc = "Success to update news";
			}
			else{
				$responseCode = "01";
				$responseDesc = "Failed to update news";
			}

		}
		else{
			$responseCode = "401";
			$responseDesc = $this->form_validation->error_array();
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);
	}

	public function deleteNews_delete($newsId)
	{

		$query = $this->mn->delete_news($newsId);

		if ($query) {
			$responseCode = "00";
			$responseDesc = "Success to delete news";
			$responseData = null;
		}

		else{
			$responseCode = "01";
			$responseDesc = "Failed to delete news";
			$responseData = null;
		}
		

		$response = resultJson( $responseCode, $responseDesc, $responseData);

		$this->response($response, 200);

	}




}
